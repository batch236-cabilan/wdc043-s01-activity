package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you?");

        //1st method
            // using the parse double to convert the scanner object value to double.
//        double age = Double.parseDouble(userInput.nextLine());

        //2nd method
        double age = userInput.nextDouble();
        System.out.println("This is a confirmation that you are "+ age +" years old."); // a method of Java Scanner which is used to scan the next token of the input as a double.
        // The Scanner object has multiple methods that can be used to get the expected value:

            // nextInt() - expects an integer
            // nextDouble() - expects a double
            // nextLine() - gets the entire line as a String

        // Much like JS, Java also has similar mathematical operators
        System.out.println("Enter a first number");
        int num1 = userInput.nextInt();

        System.out.println("Enter a second number");
        int num2 = userInput.nextInt();

        int sum = num1  + num2;
        System.out.println("The sum of both number are: " +sum);
    }
}
