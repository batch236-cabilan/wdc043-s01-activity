package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        // We instantiate the myObj form the Scanner class
        // Scanner is used for obtaining input from the terminal.
        // "Sytem.in" allows us to takes input from the console.
        Scanner myObj = new Scanner(System.in);

        System.out.println("Enter username: ");

        // to capture the input given by the user, we use the nextLine() method.
        // takes an input until the line change or new line and ends input of gets enter.
        String userName = myObj.nextLine();
        System.out.println("Username is: " + userName);

        // The expected input is string, so if we will perform the "+" operation it will output the concatenated output.
//        System.out.println(userName * userName);
    }
}
